# Progetto_Malnati

# 2018-2019

# Sign Up Trello
    Ragazzi, iscrivetevi a Trello (www.trello.com) e fate il join della bacheca (https://trello.com/invite/b/ouGkn0Pt/bf8220fd342ce64189ccb1ffb05f17e5/progettomalnati). E' un comodo modo per organnizare i task.

# STRUTTURA CARTELLE

# Le cartelle vanno organizzate in modo piuttosto coerente.
# È importante non mischiare tutto in poche cartelle confuse specialmente per progetti destinati a crescere rapidamente.
# Una struttura minimale è la seguente:

# src – La cartella src contiene il codice sorgente. Il codice al suo interno può essere organizzato come si desidera in accordo con le convenzioni del linguaggio. L’importante è che contenga solamente il codice del programma/libreria.
# test – La cartella test contiene il codice sorgente dedicato ai test. Il codice di test serve a provare il corretto funzionamento delle classi sviluppate o di alcuni moduli software oppure possono essere semplicemente test prestazionali.
# build – La cartella build contiene tutti i file ottenuti come residuo di una compilazione. Possono essere moduli parziali C/C++ (.o). La cartella va a sua volta divisa nelle varie configurazioni di compilazione (ad esempio ci potrebbe essere una configurazione Debug con compilati ottimizzati per il debug e una configurazione Release con compilati ottimizzati per le prestazioni).
# dist – La cartella dist contiene il prodotto finito. Può essere il programma eseguibile. Tutto quello che sta in dist deve essere pronto per essere eseguito e distribuito.
# doc – La cartella doc contiene la documentazione dettagliata. Tale documentazione è per lo più la specifica dettagliata delle API la quale è solitamente auto-generata.

# Da versionare su GIT:
# src, test, doc e le configurazioni per i buildati.

